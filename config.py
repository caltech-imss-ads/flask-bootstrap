import os

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Secret key for session management. You can generate random strings here:
# http://clsc.net/tools-old/random-string-generator.php
SECRET_KEY = 'my precious'

try:
    import pwd
    FULL_NAME = os.environ.get('FLASK_FULL_NAME', pwd.getpwnam(os.getlogin())[4] or "Pat Public")
except ImportError:
    # Not on a Linux or Mac machine
    FULL_NAME = os.environ.get('FLASK_FULL_NAME', os.getlogin())


SITE_NAME = os.environ.get('FLASK_SITE_NAME', 'CS/EN 87: {}'.format(FULL_NAME))
