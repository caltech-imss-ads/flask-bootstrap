VERSION = 1.1.0

PACKAGE = flask-bootstrap

#======================================================================


clean:
	rm -rf *.tar.gz dist *.egg-info *.rpm *.xml pylint.out
	find . -name "*.pyc" -exec rm '{}' ';'

version:
	@echo ${VERSION}

dist: clean
	@(cd ..; zip -r flask-bootstrap.zip flask-bootstrap -x *.git* -x bitbucket-pipelines.yml -x Makefile)
