from flask_wtf import FlaskForm
from wtforms import TextAreaField
from wtforms.validators import DataRequired

# Set your classes here.


class InputForm(FlaskForm):
    input = TextAreaField(
        'Input some text', validators=[DataRequired()]
    )
