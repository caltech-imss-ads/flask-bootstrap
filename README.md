## Welcome

Hello. Want to get started with Flask quickly? Good. You came to the right
place. This Flask application framework is pre-configured with the [Flask-WTF](https://flask-wtf.readthedocs.io/en/stable/)
forms management library and the [Bootstrap](https://getbootstrap.com/) CSS/Javascript frontend (among others). This will get your
Flask app up and running easily.

-----------------

**What is Flask?** Flask is a microframework for Python based on Werkzeug and Jinja2.

Project Structure
-----------------

    ├── README.md
    ├── app.py
    ├── config.py
    ├── error.log
    ├── forms.py
    ├── requirements.txt
    ├── bin
    │   └── service
    ├── static
    │   ├── css
    │   │   ├── bootstrap-3.3.7.css
    │   │   ├── bootstrap-3.3.7.min.css
    │   │   ├── bootstrap-theme-3.3.7.css
    │   │   ├── bootstrap-theme-3.3.7.min.css
    │   │   ├── font-awesome-4.1.0.min.css
    │   │   ├── layout.forms.css
    │   │   ├── layout.main.css
    │   │   ├── main.css
    │   │   ├── main.quickfix.css
    │   │   └── main.responsive.css
    │   ├── font
    │   │   ├── FontAwesome.otf
    │   │   ├── fontawesome-webfont.eot
    │   │   ├── fontawesome-webfont.svg
    │   │   ├── fontawesome-webfont.ttf
    │   │   ├── fontawesome-webfont.woff
    │   │   ├── glyphicons-halflings-regular.eot
    │   │   ├── glyphicons-halflings-regular.svg
    │   │   ├── glyphicons-halflings-regular.ttf
    │   │   ├── glyphicons-halflings-regular.woff
    │   │   └── glyphicons-halflings-regular.woff2
    │   ├── ico
    │   │   ├── apple-touch-icon-114-precomposed.png
    │   │   ├── apple-touch-icon-144-precomposed.png
    │   │   ├── apple-touch-icon-57-precomposed.png
    │   │   ├── apple-touch-icon-72-precomposed.png
    │   │   └── favicon.png
    │   ├── img
    │   └── js
    │       ├── libs
    │       │   ├── bootstrap-3.3.7.min.js
    │       │   ├── jquery-1.11.1.min.js
    │       │   ├── modernizr-2.8.2.min.js
    │       │   └── respond-1.4.2.min.js
    │       ├── plugins.js
    │       └── script.js
    └── templates
        ├── errors
        │   ├── 404.html
        │   └── 500.html
        ├── forms
        │   ├── input.html
        ├── layouts
        │   ├── form.html
        │   └── main.html
        └── pages
            ├── about.html
            ├── home.html
            └── post.html

### Set up your own computer to work on the app

1. Get the repo:

```
    $ curl -O https://s3.amazonaws.com/imss/flask-bootstrap-1.1.0.zip
    $ unzip flask-bootstrap-1.1.0.zip
    $ cd flask-bootstrap-1.1.0
```

2. Initialize and activate a virtualenv:

```
    $ virtualenv --no-site-packages env
    $ source env/bin/activate
```

3. Install the dependencies:

```
    $ pip install -r requirements.txt
```

5. Run the development server:

```
    $ export FLASK_APP=app.py
    $ flask run
```

6. Navigate to [http://localhost:5000](http://localhost:5000)

### Adding your own code

Look in `app.py` at the `home()` and `post()` functions.

If you have additional python modules to install while writing your code, add
them to `requirements.txt`.

### Running your application on the Linux server

1. Copy any changes you made to your code into the `app` folder in your account
   on the server.

2. Install any new dependencies for your code

```
    $ cd app
    $ source ~/.ve/bin/activate
    $ pip install --upgrade -r requirements.txt
```

3. Restart the server

```
    $ bin/service restart
```

### Learn More

1. [Flask Quickstart](http://flask.pocoo.org/docs/0.12/quickstart/)
1. [Flask Documentation](http://flask.pocoo.org/docs/0.12)
1. [Flask Extensions](http://flask.pocoo.org/extensions/)
1. [Flask-WTF Documentation](https://flask-wtf.readthedocs.io/en/stable/)
1. [Bootstrap](https://getbootstrap.com/)