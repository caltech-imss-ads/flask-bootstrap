from flask import Flask, render_template, request
import logging
from logging import Formatter, FileHandler
from forms import InputForm
import os
import config

# ----------------------------------------------------------------------------#
# App Config.
# ----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')

# ----------------------------------------------------------------------------#
# Controllers.
# ----------------------------------------------------------------------------#


@app.context_processor
def inject_boilerplate():
    return dict(site_name=config.SITE_NAME, full_name=config.FULL_NAME)


@app.route('/')
def home():
    """
    This is the code that's executed to display the form on the home page.

    If you want to change the form, edit ``InputForm`` appropriately.
    """
    form = InputForm(request.form)
    return render_template('pages/home.html', form=form)


@app.route('/post', methods=['POST'])
def post():
    """
    Here is where you'll do the processing on the form you submitted.

    ``request.form`` has all the form data, and if you're using the
    original ``InputForm``, your data is all in ``request.form['input']``.
    """

    data = request.form['input']

    # process data here

    return render_template('pages/post.html', data=data)


@app.route('/about')
def about():
    """
    If you want to say anything about what you're doing in this app, do it
    by editing the ``pages/about.html`` template.
    """
    return render_template('pages/about.html')


# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    app.run(host='0.0.0.0', port=port)
