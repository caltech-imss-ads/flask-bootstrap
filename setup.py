#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="flask-bootstrap",
    version="1.1.0",
    description="A basic flask project with Bootstrap",
    author="IMSS ADS",
    author_email="imss-ads-staff@caltech.edu",
    packages=find_packages(),
    include_package_data=True,
    scripts=['bin/service']
)
